var express 	= require('express');
var exSession 	= require('express-session');
var bodyParser 	= require('body-parser');
var app 		= express();
var login = require('./controllers/login');
var home = require('./controllers/home/home')
var reg = require('./controllers/registration/registration');
var session = require('express-session');
var message = require('./controllers/user/message/message');
const cookieParser = require('cookie-parser');

app.set('view engine', 'ejs');
app.use(cookieParser());
app.use(bodyParser.urlencoded({extended: false}));
app.use(exSession({secret: 'my secret value', saveUninitialized: true, resave: false}));
app.use(bodyParser.urlencoded({extended: false}));
app.use(exSession({secret: 'my secret value', saveUninitialized: true, resave: false}));
app.use('/message',message);
app.use('/assets',express.static('assets'));

app.listen(25565, function(){
    console.log('express http server started at...25565');
    
});

app.get('/', function(req, res){
    res.redirect('/login');
});
app.use('/login',login);
app.use('/home',home);
app.use('/registration',reg);
