var express = require('express');
var logindb      = require.main.require('./models/login');
var router = express.Router();

router.get('/', function(req, res){
    if(req.cookies['username'] == null){
        res.render('login/index');
    }
    else{
        res.redirect('/home')
    }
	
});
router.post('/', function(req, res){
    var username = req.body.username;
    var password = req.body.password;
    logindb.getCredentials(username,password,function(result){
        if(result.length>0){
            if(result[0].password==password){
                var remember = req.body.remember;
                if(remember=="true"){
                    res.cookie('username',result[0].username,{ maxAge: 60000, httpOnly: true });
                    res.cookie('userid',result[0].pid,{ maxAge: 60000, httpOnly: true });
                    res.cookie('type',result[0].type,{ maxAge: 60000, httpOnly: true });
                    
                }
                else{
                    res.cookie('username',result[0].username);
                    res.cookie('userid',result[0].pid);
                    res.cookie('type',result[0].type);
                }
                
                res.json({ status: 'valid' });
            } 
            else{
                res.json({ status: 'invalid' });
            }
        }
        else{
            res.json({ status: 'invalid' });
        }
    })
    
});

module.exports = router;