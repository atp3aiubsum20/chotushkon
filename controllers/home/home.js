var express = require('express');
var db      = require.main.require('./models/dbconnection');
var router = express.Router();

router.get('/', function(req, res){

    if(req.cookies['username'] != null){
        if(req.cookies['type']=="member")
        {
            var username=req.cookies['username'];
            var userid=req.cookies['userid'];
            var data= {
                userdata: [username,userid]
            }
            res.render('home/index',data);
        }
        else{
            res.redirect('/admin');
        }
        
	}else{
		res.redirect('/login');
	}
});

module.exports = router;